package com.baidak.softserve.elementary;

import java.util.ArrayList;
import java.util.List;

public class ChessLine {
    List<Character> line = new ArrayList<>();

    ChessLine(int length){
        for (int i = 1; i <= length; i++){
            if(i%2 == 1){
                addChar('*');
            }else {
                addChar(' ');
            }
        }
    }

    private void addChar(char symbol) {
        line.add(symbol);
    }

    private String arrayToString() {
        StringBuilder sb = new StringBuilder();

        for (Character ch : line) {
            sb.append(ch);
        }
        return sb.toString();
    }


    @Override
    public String toString() {
        return arrayToString();
    }
}
