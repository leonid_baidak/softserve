package com.baidak.softserve.elementary;

public class Main {

    public static void main(String[] args) {
        try {
            int num1 = Integer.parseInt(args[0]);
            int num2 = Integer.parseInt(args[1]);
            if (num1 > 0 && num2 > 0) {
                ChessBoard myBoard = new ChessBoard(num1, num2);
                myBoard.toPrint();
            }
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
            System.out.println("There is some error. Please, try again");
        }

    }

}
