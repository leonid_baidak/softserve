package com.baidak.softserve.elementary;

import java.util.ArrayList;
import java.util.List;

public class ChessBoard {

    List<String> lines = new ArrayList<>();

    ChessBoard(int length, int width) {
        ChessLine line = new ChessLine(length);
        for (int i = 1; i <= width; i++) {
            if (i % 2 == 1) {
                lines.add(line.toString());
            } else {
                lines.add(" " + line.toString());
            }
        }
    }

    public void toPrint() {
        for(String str : lines){
            System.out.println(str);
        }
    }

}
