package com.baidak.softserve.elementary;

public class Calculate {

    private static int i = -10;
    private static int pow = 0;
    private static int condition = 0;

    public static void parseInt(String[] arguments) {

        condition = Integer.parseInt(arguments[0]);
        pow = Integer.parseInt(arguments[1]);
    }

    private static double powFunction(double operand, int pow) {
        double result = 1;
        if (pow > 0) {
            for (int i = 0; i < pow; i++) {
                result *= operand;
            }
        } else if (pow < 0) {
            for (int i = 0; i > pow; i--) {
                result /= operand;
            }
        }
        return result;
    }

    public static void printNumbers() {
        while (true) {
            if (powFunction(i, pow) < condition) {
                System.out.print(i + ", ");
                i++;
            } else return;
        }
    }


}
