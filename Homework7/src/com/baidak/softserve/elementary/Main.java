package com.baidak.softserve.elementary;

public class Main {

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("Instruction!");
            return;
        } else if (args.length > 2) {
            System.out.println("Too much arguments!");
            return;
        }

        try {
            Calculate.parseInt(args);
            Calculate.printNumbers();
        } catch (NumberFormatException ex) {
            System.out.println("You have entered wrong number format! You must enter a positive integer.");
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("You haven't entered any parameter. You must enter a positive integer.");
        }


    }
}
