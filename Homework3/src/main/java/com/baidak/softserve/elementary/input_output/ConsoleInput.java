package com.baidak.softserve.elementary.input_output;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ConsoleInput extends AbstractInput {
    Scanner sc = new Scanner(System.in);

    @Override
    public String choseFigure() {
        return sc.nextLine();
    }

    @Override
    public String decisionToContinue() {
        return sc.nextLine();
    }

    @Override
    public List<String> getFigureParameters() {
        String figure = sc.nextLine().replaceAll("\\s+", "");
        return new ArrayList<>(Arrays.asList(figure.split(",")));
    }
}
