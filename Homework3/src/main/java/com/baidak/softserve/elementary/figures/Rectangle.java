package com.baidak.softserve.elementary.figures;

public class Rectangle extends Quadrangle {

    public Rectangle(String name, double side1, double side2) {

        this.name = name;
        sides.add(side1);
        sides.add(side2);
        setHalfPerimeter();
        setArea();
    }

    @Override
    void setArea() {
        area = sides.get(0) * sides.get(1);
    }

    @Override
    void setHalfPerimeter() {
        halfPerimeter = (sides.get(0) + sides.get(1));
    }
}
