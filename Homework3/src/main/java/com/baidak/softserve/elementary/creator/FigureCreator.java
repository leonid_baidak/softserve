package com.baidak.softserve.elementary.creator;

import com.baidak.softserve.elementary.figures.*;

import java.util.ArrayList;
import java.util.List;

public class FigureCreator {

    public static GeometricFigure createFigure(String decision, List<String> params) {
        String name = params.remove(0);
        List<Double> sides = parseSides(params);
        GeometricFigure figure = new Triangle("default", 1, 1, 1);
        if (decision.equalsIgnoreCase("t")) {
            figure = new Triangle(name, sides.get(0), sides.get(1), sides.get(2));
        } else if (decision.equalsIgnoreCase("q")) {
            figure = new Quadrangle(name, sides.get(0), sides.get(1), sides.get(2));
        } else if (decision.equalsIgnoreCase("r")) {
            figure = new Rectangle(name, sides.get(0), sides.get(1));
        } else if (decision.equalsIgnoreCase("s")) {
            figure = new Square(name, sides.get(0));
        }
        return figure;
    }

    private static List<Double> parseSides(List<String> params) {
        List<Double> sides = new ArrayList<>();
        for (String str : params) {
            sides.add(Double.parseDouble(str));
        }
        return sides;
    }
}
