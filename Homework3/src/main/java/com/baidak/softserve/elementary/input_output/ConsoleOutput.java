package com.baidak.softserve.elementary.input_output;

import com.baidak.softserve.elementary.figures.GeometricFigure;

import java.util.List;

public class ConsoleOutput extends AbstractOutput {

    @Override
    public void showOptions() {
        System.out.println("Choose the type of figure: t for triangle, q for quadrangle( r for rectangle" +
                " and s for square).");
    }

    @Override
    public void showAdvice() {
        System.out.println("Enter figure parameters.");
    }

    @Override
    public void showSomeInfo(String str) {
        System.out.println(str);
    }

    @Override
    public void showDecisionToContinue() {
        System.out.println("Do you want to continue adding figures? If it is so enter yes. Otherwise, "
                + "to show result, type anything else.");
    }

    @Override
    public void showResult(List<GeometricFigure> figures) {
        int i = 0;
        for (GeometricFigure figure : figures) {
            System.out.println(++i + ": Figure type is " + figure.getClass().getSimpleName()
                    + ", it's name is \"" + figure.getName() + "\" and area is "
                    + figure.getArea() + " square units.");
        }
    }

    @Override
    public void showAdviceForTriangle() {
        System.out.println("4 params for triangle - name and 3 sides.");
    }

    @Override
    public void showAdviceForQuadrangle() {
        System.out.println("4 params for quadrangle - name, 2 diagonals and angle between them.");
    }

    @Override
    public void showAdviceForRectangle() {
        System.out.println("3 params for rectangle - name and 2 sides.");
    }

    @Override
    public void showAdviceForSquare() {
        System.out.println("2 params for square - name and side.");
    }

    @Override
    public void printResult(List<GeometricFigure> figures) {
        if (figures.isEmpty()) {
            System.out.println(("There is nothing to show - you haven't added any figure"));
        } else {
            showResult(figures);
        }
    }

}
