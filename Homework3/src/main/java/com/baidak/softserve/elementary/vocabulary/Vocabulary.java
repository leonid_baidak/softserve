package com.baidak.softserve.elementary.vocabulary;

import java.util.HashMap;
import java.util.Map;

public class Vocabulary {

    public static Map<String, String> figuresVocabulary = new HashMap<>();

    static {
        figuresVocabulary.put("t", "4 params for triangle - name and 3 sides.");
        figuresVocabulary.put("q", "4 params for quadrangle - name, 2 diagonals and angle between them.");
        figuresVocabulary.put("r", "3 params for rectangle - name and 2 sides.");
        figuresVocabulary.put("s", "2 params for square - name and side.");
    }
}
