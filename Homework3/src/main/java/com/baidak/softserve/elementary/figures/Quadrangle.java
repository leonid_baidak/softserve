package com.baidak.softserve.elementary.figures;

public class Quadrangle extends GeometricFigure {

    public Quadrangle() {
    }

    public Quadrangle(String name, double firstDiagonal, double secondDiagonal, double angleBetweenDiagonals) {

        this.name = name;
        sides.add(firstDiagonal);
        sides.add(secondDiagonal);
        this.angleBetweenDiagonals = angleBetweenDiagonals;
        setHalfPerimeter();
        setArea();
    }

    @Override
    void setArea() {
        area = Math.sin(Math.toRadians(angleBetweenDiagonals)) * (sides.get(0) * sides.get(1) * 0.5);
    }

    @Override
    void setHalfPerimeter() {
    }

}
