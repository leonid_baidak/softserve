package com.baidak.softserve.elementary.comparing;

import com.baidak.softserve.elementary.figures.GeometricFigure;

import java.util.Comparator;

public class FiguresComparator implements Comparator<GeometricFigure> {

    @Override
    public int compare(GeometricFigure o1, GeometricFigure o2) {
        int AreaCompare = o1.getArea().compareTo(o2.getArea());
        int NameCompare = o2.getName().compareTo(o1.getName());

        if (AreaCompare != 0) {
            return AreaCompare;
        } else return NameCompare;
    }
}
