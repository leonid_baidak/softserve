package com.baidak.softserve.elementary.input_output;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractInput {
    public List<String> getFigureParameters() {
        return new ArrayList<>();
    }

    public abstract String choseFigure();

    public abstract String decisionToContinue();


}
