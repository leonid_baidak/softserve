package com.baidak.softserve.elementary;

import com.baidak.softserve.elementary.comparing.FiguresComparator;
import com.baidak.softserve.elementary.creator.FigureCreator;
import com.baidak.softserve.elementary.figures.GeometricFigure;
import com.baidak.softserve.elementary.input_output.AbstractInput;
import com.baidak.softserve.elementary.input_output.AbstractOutput;
import com.baidak.softserve.elementary.input_output.ConsoleInput;
import com.baidak.softserve.elementary.input_output.ConsoleOutput;
import com.baidak.softserve.elementary.validator.Validator;
import com.baidak.softserve.elementary.vocabulary.Vocabulary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class Main {
    static AbstractInput input = new ConsoleInput();
    static AbstractOutput output = new ConsoleOutput();

    public static void main(String[] args) {

        List<GeometricFigure> figures = new ArrayList<>();
        output.showSomeInfo("Hello there!");

        while (true) {
            output.showOptions();
            String choice = input.choseFigure();

            if (Validator.checkChoice(choice)) {

                output.showAdvice();
                output.showSomeInfo(Vocabulary.figuresVocabulary.get(choice));

                List<String> arguments = input.getFigureParameters();

                if (Validator.validateParameters(choice, arguments)) {
                    figures.add(FigureCreator.createFigure(choice, arguments));
                    output.showSomeInfo("You've successfully added the figure.");
                }
            }
            output.showDecisionToContinue();
            String decision = input.decisionToContinue();
            if ( Pattern.matches("(?i)(ye?s?)", decision)) {
                output.showSomeInfo("Ok, we'll continue.");
            } else {
                break;
            }
        }
        figures.sort(Collections.reverseOrder(new FiguresComparator()));
        output.printResult(figures);
    }
}

