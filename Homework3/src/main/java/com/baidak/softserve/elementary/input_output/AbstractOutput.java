package com.baidak.softserve.elementary.input_output;

import com.baidak.softserve.elementary.figures.GeometricFigure;

import java.util.List;

public abstract class AbstractOutput {
    public abstract void showOptions();

    public abstract void showAdvice();

    public abstract void showSomeInfo(String str);

    public abstract void showDecisionToContinue();

    public abstract void showResult(List<GeometricFigure> figures);

    public abstract void showAdviceForTriangle();

    public abstract void showAdviceForQuadrangle();

    public abstract void showAdviceForRectangle();

    public abstract void showAdviceForSquare();

    public abstract void printResult(List<GeometricFigure> figures);

}
