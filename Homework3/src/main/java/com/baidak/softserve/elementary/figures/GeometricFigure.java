package com.baidak.softserve.elementary.figures;

import java.util.ArrayList;
import java.util.List;

public abstract class GeometricFigure {
    Double area;
    String name;
    double halfPerimeter;
    double angleBetweenDiagonals;
    List<Double> sides = new ArrayList<>();

    abstract void setArea();

    abstract void setHalfPerimeter();

    public Double getArea() {
        return area;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "GeometricFigure{" +
                "area=" + area +
                ", name='" + name + '\'' +
                '}';
    }
}
