package com.baidak.softserve.elementary.figures;

import static java.lang.Math.sqrt;

public class Triangle extends GeometricFigure {

    public Triangle(String name, double side1, double side2, double side3) {

        this.name = name;
        sides.add(side1);
        sides.add(side2);
        sides.add(side3);
        setHalfPerimeter();
        setArea();
    }

    @Override
    void setArea() {
        area = sqrt(halfPerimeter * (halfPerimeter - sides.get(0)) * (halfPerimeter - sides.get(1)) *
                (halfPerimeter - sides.get(2)));
    }

    @Override
    void setHalfPerimeter() {
        halfPerimeter = (sides.get(0) + sides.get(1) + sides.get(2)) / 2;
    }
}
