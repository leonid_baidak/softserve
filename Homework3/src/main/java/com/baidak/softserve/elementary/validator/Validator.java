package com.baidak.softserve.elementary.validator;

import com.baidak.softserve.elementary.input_output.AbstractOutput;
import com.baidak.softserve.elementary.input_output.ConsoleOutput;
import com.baidak.softserve.elementary.vocabulary.Vocabulary;

import java.util.List;

public class Validator {

    static double side1 = 0;
    static double side2 = 0;
    static double side3 = 0;
    static AbstractOutput output = new ConsoleOutput();
    static String inputError = "Something went wrong. Most likely you've entered the wrong parameters.";

    public static boolean validateParameters(String choice, List<String> arguments) {
        try {
            if (choice.equalsIgnoreCase("t") && arguments.size() == 4) {
                output.showSomeInfo("You want to create Triangle.");
                return validateParamsForTriangle(arguments);

            } else if (choice.equalsIgnoreCase("q") && arguments.size() == 4) {
                output.showSomeInfo("You want to create Quadrangle.");
                return validateParamsForQuadrangle(arguments);

            } else if (choice.equalsIgnoreCase("r") && arguments.size() == 3) {
                output.showSomeInfo("You want to create Rectangle.");
                return validateParamsForRectangle(arguments);

            } else if (choice.equalsIgnoreCase("s") && arguments.size() == 2) {
                output.showSomeInfo("You want to create Square.");
                return validateParamsForSquare(arguments);

            } else {
                output.showSomeInfo("I don't know how to create this geometric figure.");
            }
            output.showSomeInfo(inputError);
            return false;
        } catch (NumberFormatException ex) {
            output.showSomeInfo("Values for sides must be numerical!");
            return false;
        }
    }

    public static boolean checkChoice(String str) {
        if (Vocabulary.figuresVocabulary.containsKey(str)) {
            return true;
        } else {
            output.showSomeInfo("Wrong choice - i can't create this figure");
            return false;
        }
    }

    private static boolean validateParamsForTriangle(List<String> arguments) {
        side1 = Double.parseDouble(arguments.get(1));
        side2 = Double.parseDouble(arguments.get(2));
        side3 = Double.parseDouble(arguments.get(3));
        boolean conditionForExistingTriangles = (side1 + side2 > side3
                && side3 + side2 > side1 && side1 + side3 > side2);

        if (side1 > 0 && side2 > 0 && side3 > 0 && conditionForExistingTriangles) {
            return true;
        } else {
            output.showSomeInfo(inputError);
            return false;
        }
    }

    private static boolean validateParamsForRectangle(List<String> arguments) {
        side1 = Double.parseDouble(arguments.get(1));
        side2 = Double.parseDouble(arguments.get(2));
        if (side1 > 0 && side2 > 0) {
            return true;
        } else {
            output.showSomeInfo(inputError);
            return false;
        }
    }

    private static boolean validateParamsForSquare(List<String> arguments) {
        side1 = Double.parseDouble(arguments.get(1));
        if (side1 > 0) {
            return true;
        } else {
            output.showSomeInfo(inputError);
            return false;
        }
    }

    private static boolean validateParamsForQuadrangle(List<String> arguments) {
        side1 = Double.parseDouble(arguments.get(1));
        side2 = Double.parseDouble(arguments.get(2));
        double angleBetweenDiagonals = Double.parseDouble(arguments.get(3));
        if (side1 > 0 && side2 > 0 && angleBetweenDiagonals > 0 && angleBetweenDiagonals < 180) {
            return true;
        } else {
            output.showSomeInfo(inputError);
            return false;
        }
    }


}
