package com.baidak.softserve.elementary.creator;

import com.baidak.softserve.elementary.figures.*;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FigureCreatorTest {


    @Test
    public void triangleCreatorTest() {
        //GIVEN
        GeometricFigure expected = new Triangle("test", 1, 1, 1);
        List<String> testParams = new ArrayList<>();
        testParams.add("test");
        testParams.add("1");
        testParams.add("1");
        testParams.add("1");
        //WHEN
        GeometricFigure actual = FigureCreator.createFigure("t", testParams);
        //THEN
        Assertions.assertThat(actual).isEqualToComparingFieldByField(expected);
    }

    @Test
    public void quadrangleCreatorTest() {
        //GIVEN
        GeometricFigure expected = new Quadrangle("test", 13,
                17, 100);
        List<String> testParams = new ArrayList<>();
        testParams.add("test");
        testParams.add("13");
        testParams.add("17");
        testParams.add("100");
        //WHEN
        GeometricFigure actual = FigureCreator.createFigure("q", testParams);
        //THEN
        Assertions.assertThat(actual).isEqualToComparingFieldByField(expected);
    }

    @Test
    public void rectangleCreatorTest() {
        //GIVEN
        GeometricFigure expected = new Rectangle("test", 13, 17);
        List<String> testParams = new ArrayList<>();
        testParams.add("test");
        testParams.add("13");
        testParams.add("17");
        //WHEN
        GeometricFigure actual = FigureCreator.createFigure("r", testParams);
        //THEN
        Assertions.assertThat(actual).isEqualToComparingFieldByField(expected);
    }

    @Test
    public void squareCreatorTest() {
        //GIVEN
        GeometricFigure expected = new Square("test", 13);
        List<String> testParams = new ArrayList<>();
        testParams.add("test");
        testParams.add("13");
        //WHEN
        GeometricFigure actual = FigureCreator.createFigure("s", testParams);
        //THEN
        Assertions.assertThat(expected).isEqualToComparingFieldByField(actual);
    }

    @Test
    public void defaultCreatorTest() {
        //GIVEN
        GeometricFigure expected = new Triangle("default", 1,1,1);
        List<String> testParams = new ArrayList<>();
        testParams.add("wrong");
        testParams.add("10");
        //WHEN
        GeometricFigure actual = FigureCreator.createFigure("w", testParams);
        //THEN
        Assertions.assertThat(expected).isEqualToComparingFieldByField(actual);
    }
}
