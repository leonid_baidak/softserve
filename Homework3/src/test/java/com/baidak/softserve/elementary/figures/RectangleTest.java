package com.baidak.softserve.elementary.figures;

import org.junit.Assert;
import org.junit.Test;

public class RectangleTest {

    @Test
    public void AreaTest() {
        //GIVEN
        double expectedArea = 10.5;
        GeometricFigure rectangle = new Rectangle("test", 1.5, 7);
        //WHEN
        double actualArea = rectangle.getArea();
        //THEN
        Assert.assertEquals(expectedArea, actualArea, 0);
    }
}
