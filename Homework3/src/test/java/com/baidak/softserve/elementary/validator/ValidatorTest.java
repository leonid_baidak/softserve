package com.baidak.softserve.elementary.validator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

@RunWith(value = Parameterized.class)
public class ValidatorTest {

    private String choice;
    private List<String> arguments;

    public ValidatorTest(String choice, List<String> args) {
        this.choice = choice;
        this.arguments = args;
    }

    @Parameterized.Parameters
    public static List<?> primeNumbers() {
        return Arrays.asList(new Object[][]{
                {"t",  Arrays.asList("Triangle", "3", "4", "5")},
                {"q", Arrays.asList("Quadrangle", "10", "20", "120")},
                {"r", Arrays.asList("Rectangle", "10", "20")},
                {"s", Arrays.asList("Square", "10")},
        });
    }

    @Test
    public void validateParametersTest() {
      Assert.assertTrue(Validator.validateParameters(this.choice, this.arguments));
    }

    @Test
    public void validateWrongChoiceTest() {
        //Given
        String wrongChoice = "awd";
        //When
        Assert.assertFalse(Validator.validateParameters(wrongChoice, this.arguments));
    }

    @Test
    public void validateTooMuchParametersTest() {
        //Given
        List<String> wrongParams = Arrays.asList("Too", "much", "arguments", "for any", "case");
        //When
        Assert.assertFalse(Validator.validateParameters(this.choice, wrongParams));
    }

    @Test
    public void validateWrongParametersTest() {
        //Given
        List<String> wrongParams = Arrays.asList("Wrong", "parameters", "can't", "parse");
        //When
        Assert.assertFalse(Validator.validateParameters(this.choice, wrongParams));
    }
}
