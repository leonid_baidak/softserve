package com.baidak.softserve.elementary.figures;

import org.junit.Assert;
import org.junit.Test;

public class QuadrangleTest {

    @Test
    public void AreaTest() {
        //GIVEN
        double expectedArea = 24;
        GeometricFigure quadrangle = new Quadrangle("test", 6, 8, 90);
        //WHEN
        double actualArea = quadrangle.getArea();
        //THEN
        Assert.assertEquals(expectedArea, actualArea, 0);
    }
}
