package com.baidak.softserve.elementary.comparing;

import com.baidak.softserve.elementary.figures.GeometricFigure;
import com.baidak.softserve.elementary.figures.Rectangle;
import com.baidak.softserve.elementary.figures.Square;
import com.baidak.softserve.elementary.figures.Triangle;
import org.junit.Assert;
import org.junit.Test;

public class FiguresComparatorTest {

    private FiguresComparator figuresComparator = new FiguresComparator();

    @Test
    public void comparingByArea(){
        GeometricFigure triangle = new Triangle("a", 3,4,5);
        GeometricFigure square = new Square("b", 2);
        Assert.assertEquals(1, figuresComparator.compare(triangle, square));

        triangle = new Triangle("a", 3,4,5);
        square=new Square("b", 3);
        Assert.assertEquals(-1, figuresComparator.compare(triangle, square));

    }

    @Test
    public void comparingByName(){
        GeometricFigure rectangle = new Rectangle("a", 3, 3);
        GeometricFigure square = new Square("b", 3);
        Assert.assertEquals(1, figuresComparator.compare(rectangle, square));

        rectangle = new Rectangle("b", 3, 3);
        square = new Square("a", 3);
        Assert.assertEquals(-1, figuresComparator.compare(rectangle, square));

    }

    @Test
    public void comparingEqualFigures(){
        GeometricFigure rectangle = new Rectangle("a", 3, 3);
        GeometricFigure square = new Square("a", 3);
        Assert.assertEquals(0, figuresComparator.compare(rectangle, square));

    }


}
