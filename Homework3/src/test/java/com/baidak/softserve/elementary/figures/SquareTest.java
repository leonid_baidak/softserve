package com.baidak.softserve.elementary.figures;

import org.junit.Assert;
import org.junit.Test;

public class SquareTest {

    @Test
    public void AreaTest() {
        //GIVEN
        double expectedArea = 25;
        GeometricFigure square = new Square("test", 5);
        //WHEN
        double actualArea = square.getArea();
        //THEN
        Assert.assertEquals(expectedArea, actualArea, 0);
    }
}
