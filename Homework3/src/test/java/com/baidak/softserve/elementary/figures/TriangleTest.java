package com.baidak.softserve.elementary.figures;

import org.junit.Assert;
import org.junit.Test;

public class TriangleTest {

    @Test
    public void AreaTest() {
        //GIVEN
        double expectedArea = 6;
        GeometricFigure triangle = new Triangle("test", 3, 4, 5);
        //WHEN
        double actualArea = triangle.getArea();
        //THEN
        Assert.assertEquals(expectedArea, actualArea, 0);
    }
}
