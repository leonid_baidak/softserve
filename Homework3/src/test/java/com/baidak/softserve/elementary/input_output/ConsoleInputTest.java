package com.baidak.softserve.elementary.input_output;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

public class ConsoleInputTest {

    @Test
    public void getFigureParametersTest() {
        InputStream stdin = System.in;
        String data = "  first  ,  second  , third  ";

        System.setIn(new ByteArrayInputStream(data.getBytes()));
        AbstractInput input = new ConsoleInput();
        List<String> example = (input.getFigureParameters());

        Assert.assertTrue(example.contains("first"));
        Assert.assertTrue(example.contains("second"));
        Assert.assertTrue(example.contains("third"));
        Assert.assertFalse(example.contains(" "));
        System.setIn(stdin);
    }
}
