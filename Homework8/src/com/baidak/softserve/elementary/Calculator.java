package com.baidak.softserve.elementary;

import java.util.ArrayList;

public class Calculator {

    static long previous = 0;
    static long next = 1;
    static long result;

    static ArrayList<Long> numbers = new ArrayList<>();

    public static long fibonacciNumber() {
        result = previous + next;
        previous = next;
        next = result;
        return result;
    }

    public static void choseVariant(String[] args) {

        if (args.length == 2) {

            long num1 = stringToInteger(args[0]);
            long num2 = stringToInteger(args[1]);
            if (num1 > num2) {
                long temporary = num2;
                num2 = num1;
                num1 = temporary;
            }
            rangeOfValues(num1, num2);

        } else if (args.length == 1) {
            int num1 = stringToInteger(args[0]);
            quantityOfNumerals(num1);
        } else {
            System.out.println("You haven't entered any parameters.");
        }
    }

    public static int stringToInteger(String argument) {
        return Integer.parseInt(argument);

    }

    public static void rangeOfValues(long num1, long num2) {
        if (num1 == 0 || num1 == 1) numbers.add((long) 0);
        if (num1 == 1) numbers.add((long) 1);
        long j;
        while (true) {
            j = fibonacciNumber();
            if (j > num2) break;
            else if (j > num1)
                numbers.add(j);
        }
        print(numbers);
    }

    public static void quantityOfNumerals(int num1) {
        long j;
        while (true) {
            j = fibonacciNumber();
            if ((int) Math.log10(j) + 1 > num1) break;
            else if ((int) Math.log10(j) + 1 == num1)
                numbers.add(j);
        }
        print(numbers);
    }

    public static void print(ArrayList<Long> numbers) {
        for (Long number : numbers) System.out.print(number + ", ");
    }

}
