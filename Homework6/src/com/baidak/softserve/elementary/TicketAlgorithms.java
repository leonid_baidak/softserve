package com.baidak.softserve.elementary;

import java.util.ArrayList;
import java.util.Scanner;

public class TicketAlgorithms {

    static ArrayList<Integer> ticketsRange = new ArrayList<>();
    static int firstDigit, secondDigit, thirdDigit, fourthDigit, fifthDigit, sixthDigit, sum1, sum2;
    static String line = "---------------------------------------------------------------------------------------------";

    public static void moscowAlgorithm(ArrayList<String> tickets) {
        ArrayList<String> ticketsToPrint = new ArrayList<>();
        for (String str : tickets) {
            digitsFilling(str);
            sum1 = firstDigit + secondDigit + thirdDigit;
            sum2 = fourthDigit + fifthDigit + sixthDigit;
            if (sum1 == sum2) {
                ticketsToPrint.add(str);
            }
        }
        System.out.println("Counted by Moscow Algorithm.");
        System.out.println(line);
        print(ticketsToPrint);
    }

    public static void piterAlgorithm(ArrayList<String> tickets) {
        ArrayList<String> ticketsToPrint = new ArrayList<>();
        for (String str : tickets) {
            digitsFilling(str);
            sum1 = firstDigit + thirdDigit + fifthDigit;
            sum2 = secondDigit + fourthDigit + sixthDigit;
            if (sum1 == sum2) {
                ticketsToPrint.add(str);
            }
        }
        System.out.println("Counted by Piter Algorithm.");
        System.out.println(line);
        print(ticketsToPrint);
    }

    public static void dniproAlgorithm(ArrayList<String> tickets) {
        ArrayList<String> ticketsToPrint = new ArrayList<>();
        int digit, sum1, sum2;
        for (String str : tickets) {
            sum1 = 0;
            sum2 = 0;
            for (int i = 0; i <= 5; i++) {
                digit = Character.getNumericValue(str.charAt(i));
                if (digit % 2 == 0) sum1 += digit;
                else sum2 += digit;
            }
            if (sum1 == sum2) {
                ticketsToPrint.add(str);
            }
        }
        System.out.println("Counted by Dnipro Algorithm.");
        System.out.println(line);
        print(ticketsToPrint);
    }

    public static void digitsFilling(String line) {
        firstDigit = Character.getNumericValue(line.charAt(0));
        secondDigit = Character.getNumericValue(line.charAt(1));
        thirdDigit = Character.getNumericValue(line.charAt(2));
        fourthDigit = Character.getNumericValue(line.charAt(3));
        fifthDigit = Character.getNumericValue(line.charAt(4));
        sixthDigit = Character.getNumericValue(line.charAt(5));
    }

    public static void allAlgorithms(ArrayList<String> tickets) {
        piterAlgorithm(tickets);
        moscowAlgorithm(tickets);
        dniproAlgorithm(tickets);
    }

    public static void chooseVariant(ArrayList<Integer> tickets) {
        ticketsRange = tickets;
        ArrayList<String> ticketsStrings;
        ticketsStrings = parseToStringList(tickets);
        Scanner scan = new Scanner(System.in);
        System.out.println("You may choose from three variants: Piter, Moscow, and Dnipro. Or select all three by typing All");
        String algorithm = scan.nextLine().toLowerCase();
        System.out.println("\n\n");
        switch (algorithm) {
            case ("piter"):
                piterAlgorithm(ticketsStrings);
                break;
            case ("moscow"):
                moscowAlgorithm(ticketsStrings);
                break;
            case ("dnipro"):
                dniproAlgorithm(ticketsStrings);
                break;
            case ("all"):
                allAlgorithms(ticketsStrings);
                break;
            default:
                chooseVariant(ticketsRange);
        }
    }

    public static ArrayList<String> parseToStringList(ArrayList<Integer> tickets) {
        ArrayList<String> listOfTickets = new ArrayList<>();
        for (Integer number : tickets) {
            listOfTickets.add(Integer.toString(number));
        }
        return listOfTickets;
    }

    public static void print(ArrayList<String> validTickets) {
        for (String line : validTickets) {
            System.out.print(line + ", ");
        }
        System.out.println("\n" + line + "\n\n");
    }

}
