package com.baidak.softserve.elementary;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TicketHandler {
    static int lowerTicket;
    static int highestTicket;
    static ArrayList<Integer> ticketsRange = new ArrayList<>();

    public static void getTickets() {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Enter number of the first ticket (6-digits number)");
            int firstTicket = scan.nextInt();
            System.out.println("Enter number of the second ticket (6-digits number)");
            int secondTicket = scan.nextInt();

            if (verifyTickets(firstTicket, secondTicket)) {
                if (turnOverIfNecessaryAndSaveResult(firstTicket, secondTicket)) {
                    getTicketsRange();
                    TicketAlgorithms.chooseVariant(ticketsRange);
                }
            }
            tryAgain();
        } catch (InputMismatchException ex) {
            System.out.println("You must type a numerical value!!! Try again.");
            getTickets();
        }
    }

    public static boolean verifyTickets(int ticket1, int ticket2) {
        if (Integer.toString(ticket1).length() == 6 && Integer.toString(ticket2).length() == 6) {
            return true;
        } else {
            System.out.println("You have entered the wrong format of tickets numbers. It must contain 6 digits");
            return false;
        }
    }

    public static boolean turnOverIfNecessaryAndSaveResult(int num1, int num2) {
        if (num1 < num2) {
            lowerTicket = num1;
            highestTicket = num2;
            return true;
        } else if (num1 > num2) {
            lowerTicket = num2;
            highestTicket = num1;
            return true;
        } else {
            System.out.println("Nothing to count - those numbers are equals.");
            return false;
        }
    }

    public static void getTicketsRange() {
        int difference = highestTicket - lowerTicket;
        for (int i = 0; i <= difference; i++) {
            ticketsRange.add(lowerTicket + i);
        }
    }

    public static void tryAgain() {
        System.out.println("Would you like to try again? If you want, you should type one of those: y, yes or Yes."
                + " Otherwise, type anything else to end program.");
        Scanner scan = new Scanner(System.in);
        String decision = scan.nextLine().toLowerCase();
        if (decision.equals("y") || decision.equals("ye") || decision.equals("yes")) {
            TicketHandler.getTickets();
        } else {
            System.out.println("Goodbye)");
        }
    }
}

