package com.baidak.softserve.elementary.containers;

import java.util.ArrayList;
import java.util.List;

public abstract class Container {

    Double valueForComparison;
    String name;
    List<Double> sides = new ArrayList<>();

    abstract void setValueForComparison();

    public Double getValueForComparison() {
        return valueForComparison;
    }

    public String getName() {
        return name;
    }

}
